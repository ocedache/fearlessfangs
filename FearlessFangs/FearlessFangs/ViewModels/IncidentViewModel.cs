﻿using FearlessFangs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FearlessFangs.ViewModels
{
    public class IncidentViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public int StateOfIncident { get; set; }
        public string LocationOfIncident { get; set; }
        public DateTime DateOfIncident { get; set; }
        public string DescriptionOfIncident { get; set; }
        public List<StateOfIncident> StatesOfIncident { get; set; }
        public DateTime TimeOfIncident { get; set; }
    }
}