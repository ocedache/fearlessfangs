﻿using BotDetect.Web.Mvc;
using FearlessFangs.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FearlessFangs.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View(new IncidentViewModel()
            {
                
            });
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult ValidateUser()
        {
            return View();
        }

        [HttpPost]
        [CaptchaValidation("CaptchaCode", "ExampleCaptcha", "Incorrect CAPTCHA code, please try again.")]
        public ActionResult ValidateUser(bool captchaValid)
        {
            if (captchaValid)
            {
                return View("Index");
            }
            else
            {
                return View("ValidateUser");
            }
        }
    }
}