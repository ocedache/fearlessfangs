﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FearlessFangs.Startup))]
namespace FearlessFangs
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
